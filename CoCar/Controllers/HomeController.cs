﻿using CoCar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoCarLib.Models;

namespace CoCar.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MyMove()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        private void ConnectSession(Membre membre) 
        {
            Session["user"] = membre;
        }

        // J'aurais dû séparer le controleur en plusieurs au lieu des regions

        #region Register
        [HttpGet]
        public ActionResult Register()
        {
            RegisterViewModel svm = new RegisterViewModel();

            return View("Register", svm);
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel svm)
        {
            if (ModelState.IsValid)
            {
                if (EFInterface.isMembreExist(svm.Membre.Email) != null)
                {
                    ModelState.AddModelError("AlreadyExist", "Cette adresse email est déjà utilisée");
                    return View("Register", svm);
                }

                HomeViewModel hvm = new HomeViewModel();
                hvm.Membre = svm.Membre;
                EFInterface.AddMembre(svm.Membre);
                ConnectSession(svm.Membre);
                return View("MyMove");
            }
            else
            {
                return View("Register", svm);
            }
        }
        #endregion    

        #region Log
        [HttpGet]
        public ActionResult Login()
        {
            LoginViewModel lvm = new LoginViewModel();

            return View("Login", lvm);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            Membre membre = EFInterface.isMembreExist(model.Email, model.Password);
            if (ModelState.IsValid && membre != null)
            {
                ConnectSession(membre);
                return View("MyMove");
            }
            else
            {
                if (membre == null)
                {
                    ModelState.AddModelError("Incorrectpassword", "Ho noooo mot de passe ou email incorrect !");
                }

                return View("Login", model);
            }
        }

        public ActionResult Logout()
        {
            ConnectSession(null);
            return View("Login");
        }

        #endregion

        #region Research

        [HttpGet]
        public ActionResult Research()
        {
            ResearchViewModel rvm = new ResearchViewModel();
            return View("Research", rvm);
        }

        [HttpPost]
        public ActionResult Research(ResearchViewModel model)
        {
            return View("Research", model);
        }

        #endregion

        #region Move

        [HttpGet]
        public ActionResult AddMove()
        {
            AddMoveViewModel avm = new AddMoveViewModel();
            return View("AddMove", avm);
        }

        [HttpPost]
        public ActionResult AddMove(AddMoveViewModel avm)
        {
            if (ModelState.IsValid)
            {
                var membre = Session["user"] as CoCarLib.Models.Membre;
                avm.Move.Driver = membre;
                EFInterface.AddMove(avm.Move);

                return View("MyMove");
            }
            else
            {
                return View("AddMove", avm);
            }
        }

        [HttpGet]
        public ActionResult Move()
        {
            MoveViewModel mvm = new MoveViewModel();
            return View("Move", mvm);
        }

        [HttpPost]
        public ActionResult Move(MoveViewModel mvm)
        {
            Membre mem = Session["user"] as Membre;
            var move = Session["movetmp"] as CoCarLib.Models.Move;

            EFInterface.AddPassenger(mem, move);
            
            return View("MyMove");
        }

        #endregion
    }
}