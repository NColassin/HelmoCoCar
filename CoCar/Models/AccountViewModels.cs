﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoCar.Models
{
    public class LoginViewModel : LayoutViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email
        {
            get { return base.Membre.Email; }
            set { base.Membre.Email = value; }
        }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password
        {
            get { return base.Membre.Pass; }
            set { base.Membre.Pass = value; }
        }
    }

    public class RegisterViewModel : LayoutViewModel { }

}
