﻿using CoCarLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoCar.Models
{
    public abstract class LayoutViewModel
    {
        public Membre Membre { get; set; }
        public Move Move { get; set; }

        public LayoutViewModel() : this(new Membre()) { }

        public LayoutViewModel(Membre membre)
        {
            this.Membre = membre;
        }

        public void setMembre()
        {
            if (this.Membre == null)
                this.Membre = new Membre();
        }
    }
}