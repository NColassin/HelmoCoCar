﻿using CoCarLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoCar.Models
{
    public class HomeViewModel : LayoutViewModel
    {
        public List<Move> moveList { get; set; }

        public List<Move> getMembreMove(Membre membre)
        {
            return EFInterface.getMovesByMembre(membre);
        }
    }
}