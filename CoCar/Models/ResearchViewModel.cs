﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoCar.Models
{
    public class ResearchViewModel : LayoutViewModel
    {
        [Display(Name = "Départ")]
        public int Start
        { get; set; }

        [Display(Name = "Arrivée")]
        public int End
        { get; set; }

        [Display(Name = "Date")]
        public DateTime Date
        { get; set; }
    }
}