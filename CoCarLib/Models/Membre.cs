﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoCarLib.Models
{
    public class Membre
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Pseudo")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Mot de passe")]
        public string Pass { get; set; }

        [Required]
        [Display(Name = "Nom")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Nom de famille")]
        public string Surname { get; set; }

        public Membre(){}

        public Membre(string email, string login, string pass, string name, string surname) 
        {
            this.Email = email;
            this.Login = login;
            this.Pass = pass;
            this.Name = name;
            this.Surname = surname;
        }
    }
}
