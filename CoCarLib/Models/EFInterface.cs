﻿using CoCarLib.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoCarLib.Models
{
    public class EFInterface
    {
        private static List<Membre> MembreList;
        private static List<Move> MoveList;

        private static void setMembreList()
        {
            MembreList = MembreMapper.GetAllMembre();

            if (MembreList == null)
            {
                MembreList = new List<Membre>();
                MembreList.Add(new Membre("test1@scampi.be", "test1", "azerty", "Noel", "Colassin"));
                MembreList.Add(new Membre("test2@scampi.be", "BRdu42", "azerty", "Bob", "Razof"));
                MembreList.Add(new Membre("test3@scampi.be", "DupontS", "azerty", "Scampi", "Dupoont"));
            }
        }

        private static void setMoveList()
        {
            setMembreList();

            if (MoveList == null)
            {
                MoveList = new List<Move>();
                DateTime newDate = new DateTime(2017, 9, 7, 12,30,00);
                DateTime newDate2 = new DateTime(2017, 10, 15, 10, 15, 00);
                DateTime newDate3 = new DateTime(2017, 9, 7, 16, 00, 00);
                DateTime newDate4 = new DateTime(2017, 10, 15, 7, 10, 00);
                Move newMove = new Move(new Membre("test1@scampi.be", "test1", "azerty", "Noel", "Colassin"), 4, 4900, 4000, newDate, 2.4);
                Move newMove2 = new Move(new Membre("test2@scampi.be", "BRdu42", "azerty", "Bob", "Razof"), 2, 5900, 3500, newDate2, 5.2);
                Move newMove3 = new Move(new Membre("test3@scampi.be", "DupontS", "azerty", "Scampi", "Dupoont"), 3, 20, 5000, newDate3, 10.20);
                Move newMove4 = new Move(new Membre("test2@scampi.be", "BRdu42", "azerty", "Bob", "Razof"), 5, 4000, 4900, newDate4, 2.4);
                newMove.Id = 0;
                newMove2.Id = 1;
                newMove3.Id = 2;
                newMove4.Id = 3;

                newMove2.Passengers.Add(new Membre("test1@scampi.be", "test1", "azerty", "Noel", "Colassin"));
                newMove2.Passengers.Add(new Membre("test3@scampi.be", "DupontS", "azerty", "Scampi", "Dupoont"));
                newMove3.Passengers.Add(new Membre("test1@scampi.be", "test1", "azerty", "Noel", "Colassin"));
                newMove3.Passengers.Add(new Membre("test2@scampi.be", "BRdu42", "azerty", "Bob", "Razof"));

                MoveList.Add(newMove);
                MoveList.Add(newMove2);
                MoveList.Add(newMove3);
                MoveList.Add(newMove4);
            }
        }

        public static Membre isMembreExist(string email)
        {
            setMembreList();

            foreach (Membre membre in MembreList)
            {
                if (membre.Email.CompareTo(email) == 0)
                    return membre;
            }
            return null;
        }

        public static Membre isMembreExist(string email, string pass)
        {
            setMembreList();

            foreach (Membre membre in MembreList)
            {
                if (membre.Email.CompareTo(email) == 0 && membre.Pass.CompareTo(pass) == 0)
                    return membre;
            }
            return null;
        }

        public static void AddMembre(Membre membre)
        {
            MembreMapper.AddMembre(membre);

            //setMembreList();
            //MembreList.Add(membre);
        }

        public static void AddMove(Move move)
        {
            //MoveMapper.AddMove(move);

            setMoveList();
            move.Id = MoveList.Count;
            MoveList.Add(move);
        }

        public static List<Move> getMovesByMembre(Membre membre)
        {
            setMoveList();
            List<Move> resultat = new List<Move>();

            foreach (Move move in MoveList)
            {
                if (move.Driver.Email.CompareTo(membre.Email) == 0)
                {
                    resultat.Add(move);
                }

                foreach (Membre mem in move.Passengers)
                {
                    if (mem.Email == membre.Email)
                    {
                        resultat.Add(move);
                    }
                }
            }

            return resultat;
        }

        public static List<Move> getAllMoves()
        {
            setMoveList();

            return MoveList;
        }

        public static List<Move> getResearchedMoves(int pcStart, int pcEnd, DateTime date)
        {
            setMoveList();
            List<Move> resultat = new List<Move>();

            foreach (Move move in MoveList)
            {
                if ((move.PcStart == pcStart || pcStart == 0) && (move.PcEnd == pcEnd || pcEnd == 0) && (move.Date.Date == date.Date || date == new DateTime(0001, 01, 1, 00, 00, 00)))
                {
                    resultat.Add(move);
                }
            }
            return resultat;
        }

        public static Move getMoveById(int id)
        {
            setMoveList();

            foreach (Move move in MoveList)
            {
                if (move.Id == id)
                {
                    return move;
                }
            }
            return MoveList.ElementAt(0);

        }

        public static void AddPassenger(Membre membre, Move move)
        {
            foreach (Move move2 in MoveList)
            {
                if (move2.Id == move.Id)
                {
                    move2.Passengers.Add(membre);
                }
            }
        }

        public static bool isMembreInMove(Membre mem, Move move)
        {
            foreach (Membre membre in move.Passengers)
            {
                if (membre.Email == mem.Email)
                {
                    return false;
                }
            }

            if (move.Driver.Email == mem.Email)
            {
                return false;
            }

            return true;
        }
    }
}
