﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoCarLib.Models
{
    public class Move
    {
        public long Id { get; set; }
        public Membre Driver { get; set; }

        [Required]
        [Display(Name = "Nombre de passager")]
        public int NbPassenger { get; set; }

        public List<Membre> Passengers { get; set; }

        [Required]
        [Display(Name = "Code postal de départ")]
        public int PcStart { get; set; }

        [Required]
        [Display(Name = "Code postal d'arrivée")]
        public int PcEnd { get; set; }

        [Required]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Prix")]
        public double Prix { get; set; }

        public Move() 
        {
            Passengers = new List<Membre>();
        }

        public Move(Membre driver, int nbPass, int pcStart, int pcEnd, DateTime date, double prix)
        {
            this.Driver = driver;
            this.NbPassenger = nbPass;
            this.PcStart = pcStart;
            this.PcEnd = pcEnd;
            this.Date = date;
            this.Prix = prix;
            Passengers = new List<Membre>();
        }

        public bool isPassed()
        {
            if (Date != null)
            {
                DateTime now = DateTime.Now;

                if (DateTime.Compare(Date, now) <= 0)
                    return true;
            }
            return false;
        }

    }
}
