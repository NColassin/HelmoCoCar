﻿using CoCarLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoCarLib.EF
{
    public class MoveMapper
    {
        public static void AddMove(Move move)
        {
            try
            {
                using (DataContext context = new DataContext("CoCar-Test"))
                {
                    context.Moves.Add(move);
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static List<Move> GetAllMove()
        {
            try
            {
                using (DataContext context = new DataContext("CoCar-Test"))
                {
                    return context.Moves.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
