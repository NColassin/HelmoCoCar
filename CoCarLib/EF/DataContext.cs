﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CoCarLib.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoCarLib.EF
{
    public class DataContext : DbContext
    {
        public DbSet<Membre> Membres { get; set; }
        public DbSet<Move> Moves { get; set; }

        public DataContext() : this("CoCar") { }

        public DataContext(string connectionString) : base(connectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Membre>().HasKey<string>(u => u.Email);
            modelBuilder.Entity<Membre>().Property(u => u.Pass).IsRequired();

            modelBuilder.Entity<Move>().HasKey<long>(m => m.Id);
            modelBuilder.Entity<Move>().Property<long>(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Move>().HasMany<Membre>(m => m.Passengers);

            base.OnModelCreating(modelBuilder);
        }
    }
}
