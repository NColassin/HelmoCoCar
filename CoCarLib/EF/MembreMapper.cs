﻿using CoCarLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoCarLib.EF
{
    public class MembreMapper
    {
        public static void AddMembre(Membre membre)
        {
            try
            {
                using (DataContext context = new DataContext("CoCar-Test"))
                {
                    context.Membres.Add(membre);
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static List<Membre> GetAllMembre()
        {
            try
            {
                using (DataContext context = new DataContext("CoCar-Test"))
                {
                    return context.Membres.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static Membre GetByEmail(string email)
        {
            try
            {
                using (DataContext context = new DataContext())
                {
                    return context.Membres.First(m => m.Email == email);
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
